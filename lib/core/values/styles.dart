
import 'package:flutter/material.dart';

import 'dimens.dart';

AppBar appBar(String title,{List<Widget>? actions}){
  return AppBar(
    centerTitle: true,
    title: Text(title,style: TextStyle(fontSize: Dimens.textSize3),),
    brightness: Brightness.dark,
    backgroundColor: Colors.transparent,
    elevation: 0,
    actions: actions,
  );
}