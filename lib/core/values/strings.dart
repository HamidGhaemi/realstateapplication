class AppStrings {
  static const String APP_NAME = 'راهنمای من';
  static const String HOME = 'خانه';
  static const String SEARCH = 'جستجو';
  static const String CONFIRM = 'تایید';
  static const String COMMISSION = 'کمیسیون';
  static const String PROFILE = 'پروفایل';
  static const String SEARCH_TITLE = 'جستجوی کد ملک';
  static const String SEARCH_HINT = 'لطفا کد ملک مورد نظر را وارد کنید';
  static const String COMMISSION_TITLE = 'محاسبه کمیسیون';
  static const String CHOOSE_CITY = 'انتخاب استان و شهر';
  static const String LOGIN_FORM_TITLE = 'فرم ورود به سامانه';
  static const String REG_YOUR_ESTATE_ADS = 'ثبت آگهی ملک شما';
  static const String ENTER_TO_APP = 'ورود به سامانه';
  static const String NEXT_IN_REG_ADS = 'ادامه ثبت آگهی';
  static const String YOUR_ESTATE_TYPE = 'نوع ملک شما';
  static const String HINT_ENTER_NUMBER = 'شماره همراه | مثال 093911111987';
  static const String HINT_ENTER_PASSWORD = 'پسورد مورد نظر';
  static const String PRICE_ = 'قیمت:';
  static const String PRICE_OF_METER_ = 'متری:';

  static const String MESSAGE_LOADING = 'در حال دریافت اطلاعات...';
  static const String MESSAGE_CHOOSE_ESTATE_TYPE =
      'نوع ملک مورد نظر خود را انتخاب کنید';
  static const String MESSAGE_LOGIN_FORM_TITLE =
      'برای ورود به سامانه لطفا شماره همراه و پسورد خود را وارد کنید';
  static const String MESSAGE_CHOOSE_AREA =
      'برای انتخاب محدوده اینجا کلیک کنید';
  static const String MESSAGE_CHOOSE_BARGAIN = 'نوع معامله خود را انتخاب کنید';
  static const String MESSAGE_FORGET_PASSWORD = 'آیا پسورد خود را فراموش کرده اید ؟';
  static const String MESSAGE_WARNING_VALIDATION = 'در وارد کردن اطلاعات دقت کنید.';

  static const String CURRENCY = 'ریال';
  static const String ESTATE_X = 'طول ملک (متر)';
  static const String ESTATE_Y = 'بر ملک (متر)';

  static const String VALIDATION_SELECT_BARGAIN =
      'لطفا ابتدا نوع معامله را مشخص نمایید.';
  static const String VALIDATION_REQUIRED_FIELD =
      'لطفا مقادیر ضروری را وارد نمایید.';
}
