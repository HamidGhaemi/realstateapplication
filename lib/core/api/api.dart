class Api {
  static const String _BASE_URL = 'http://estate.abingallery.ir/api/';

  static const String BARGAINS = _BASE_URL + 'bargains';
  static const String PROPERTY = _BASE_URL + 'property';
  static const String FILTERS = _BASE_URL + 'filters';
  static const String LOGIN = _BASE_URL + 'login';
}
