import 'package:rahnamaye_man/model/key_value.dart';

class PropertyResponse {
  List<KeyValue> properties = [];

  PropertyResponse({required this.properties});

  factory PropertyResponse.fromJson(Map<String, dynamic> json) {
    List<KeyValue> properties = [];

    (json['result'] as List).forEach((element) => properties.add(KeyValue.fromJson(element)));

    return PropertyResponse(properties: properties);
  }
}
