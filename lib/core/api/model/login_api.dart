import 'package:rahnamaye_man/model/user.dart';

class LoginRequest {
  String username;
  String password;

  LoginRequest({required this.username, required this.password});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['password'] = this.password;

    return data;
  }
}

class LoginResponse {
  User user;
  String? token;

  LoginResponse({required this.user, this.token});

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(user: User.fromJson(json), token: json['token']);
  }
}
