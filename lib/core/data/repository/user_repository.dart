import 'package:rahnamaye_man/core/data/localDataSource/user_local_data_source.dart';
import 'package:rahnamaye_man/core/data/remoteDataSource/user_remote_data_source.dart';

class UserRepository {
  UserLocalDataSource _localDataSource = UserLocalDataSource();
  UserRemoteDataSource _remoteDataSource = UserRemoteDataSource();

  bool isLogin() {
    return _localDataSource.isLogin();
  }
}
