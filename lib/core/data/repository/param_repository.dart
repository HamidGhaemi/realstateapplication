import 'package:rahnamaye_man/core/api/model/bargains_api.dart';
import 'package:rahnamaye_man/core/api/model/filters_api.dart';
import 'package:rahnamaye_man/core/api/model/login_api.dart';
import 'package:rahnamaye_man/core/api/model/property_api.dart';
import 'package:rahnamaye_man/core/data/remoteDataSource/base_remote_data_source.dart';
import 'package:rahnamaye_man/core/data/remoteDataSource/param_remote_data_source.dart';
import 'package:rahnamaye_man/core/data/repository/base_repository.dart';

class ParamRepository extends BaseRepository {
  ParamRemoteDataSource _remoteDataSource = ParamRemoteDataSource();

  //<editor-fold desc="bargains">
  bargains(
    BargainsRequest request,
    ApiSuccessCallback<BargainsResponse> onSuccess,
    ApiErrorCallback onError,
  ) {
    _remoteDataSource.bargains(request, onSuccess, onError);
  }

  //</editor-fold>

  //<editor-fold desc="property">
  property(
    ApiSuccessCallback<PropertyResponse> onSuccess,
    ApiErrorCallback onError,
  ) {
    _remoteDataSource.property(onSuccess, onError);
  }

  //</editor-fold>

  //<editor-fold desc="filters">
  filters(
    FiltersRequest request,
    ApiSuccessCallback<FiltersResponse> onSuccess,
    ApiErrorCallback onError,
  ) {
    _remoteDataSource.filters(request, onSuccess, onError);
  }

  //</editor-fold>

  //<editor-fold desc="filters">
  login(
    LoginRequest request,
    ApiSuccessCallback<LoginResponse> onSuccess,
    ApiErrorCallback onError,
  ) {
    _remoteDataSource.login(request, onSuccess, onError);
  }
//</editor-fold>
}
