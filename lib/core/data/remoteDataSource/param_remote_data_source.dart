import 'package:rahnamaye_man/core/api/api.dart';
import 'package:rahnamaye_man/core/api/model/bargains_api.dart';
import 'package:rahnamaye_man/core/api/model/filters_api.dart';
import 'package:rahnamaye_man/core/api/model/login_api.dart';
import 'package:rahnamaye_man/core/api/model/property_api.dart';
import 'package:rahnamaye_man/core/data/remoteDataSource/base_remote_data_source.dart';

class ParamRemoteDataSource extends BaseRemoteDataSource {
  //<editor-fold desc="bargains">
  bargains(
    BargainsRequest request,
    ApiSuccessCallback<BargainsResponse> onSuccess,
    ApiErrorCallback onError,
  ) async {
    getDio().post(
      Api.BARGAINS,
      data: request.toJson(),
      onSuccess: onSuccess,
      onError: onError,
      parser: (p0) => BargainsResponse.fromJson(p0),
    );
  }

  //</editor-fold>

  //<editor-fold desc="property">
  property(
    ApiSuccessCallback<PropertyResponse> onSuccess,
    ApiErrorCallback onError,
  ) async {
    getDio().post(
      Api.PROPERTY,
      onSuccess: onSuccess,
      onError: onError,
      parser: (p0) => PropertyResponse.fromJson(p0),
    );
  }

//</editor-fold>

  //<editor-fold desc="filters">
  filters(
    FiltersRequest request,
    ApiSuccessCallback<FiltersResponse> onSuccess,
    ApiErrorCallback onError,
  ) async {
    getDio().post(
      Api.FILTERS,
      data: request.toJson(),
      onSuccess: onSuccess,
      onError: onError,
      parser: (p0) => FiltersResponse.fromJson(p0),
    );
  }

//</editor-fold>

  //<editor-fold desc="login">
  login(
    LoginRequest request,
    ApiSuccessCallback<LoginResponse> onSuccess,
    ApiErrorCallback onError,
  ) async {
    getDio().post(
      Api.LOGIN,
      data: request.toJson(),
      onSuccess: onSuccess,
      onError: onError,
      parser: (p0) => LoginResponse.fromJson(p0),
    );
  }
//</editor-fold>
}
