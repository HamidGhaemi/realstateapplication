import 'package:rahnamaye_man/model/key_value.dart';

class Field {
  String? id;
  String? title;
  String? subtitle;
  String? type;
  String? sort;
  bool? required;
  List<KeyValue>? items;

  String? value;
  bool? initialExpanded;

  Field({
    this.id,
    this.title,
    this.subtitle,
    this.type,
    this.sort,
    this.required,
    this.items,
    this.initialExpanded,
  });

  factory Field.fromJson(Map<String, dynamic> json) {
    List<KeyValue> items = [];

    if (json['items'] != null) {
      (json['items'] as List).forEach((element) {
        items.add(KeyValue.fromJson(element));
      });
    }

    return Field(
      id: json['id'],
      title: json['title'],
      subtitle: 'تحت عنوان',
      type: json['type'],
      sort: json['sort'],
      required: json['required'] == 'yes',
      items: items,
      initialExpanded: false,
    );
  }

  String toStr(String? value) {
    String result = '';

    if (value != null) {
      List<String> valueIds = value.split(',');
      items!.forEach((element) {
        if (valueIds.contains(element.id.toString())) {
          result = result + element.title! + ', ';
        }
      });
    }

    return result;
  }
}
