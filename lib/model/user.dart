class User {
  String? name;
  String? mobile;
  String? avatar;

  User({
    this.name,
    this.mobile,
    this.avatar,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      name: json['name'],
      mobile: json['mobile'],
      avatar: json['avatar'],
    );
  }
}
