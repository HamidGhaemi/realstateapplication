import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:rahnamaye_man/component/form_maker.dart';
import 'package:rahnamaye_man/core/api/model/filters_api.dart';
import 'package:rahnamaye_man/core/base/base_view_model.dart';
import 'package:rahnamaye_man/core/data/repository/param_repository.dart';
import 'package:rahnamaye_man/core/values/strings.dart';
import 'package:rahnamaye_man/model/field.dart';

class RegAdsStep2ViewModel extends BaseViewModel {
  ParamRepository _paramRepository = ParamRepository();

  List<Field> form = [];
  late FormMaker formMaker = FormMaker(form);

  setBargainId(String bargainId) {
    _fetchProperties(bargainId);
  }

  _fetchProperties(String bargainId) {
    EasyLoading.show(status: AppStrings.MESSAGE_LOADING);

    form.clear();

    _paramRepository.property(
      (response) {
        //<editor-fold desc="estate type">
        form.add(Field(
          id: '0',
          type: 'radio',
          title: AppStrings.YOUR_ESTATE_TYPE,
          subtitle: AppStrings.MESSAGE_CHOOSE_ESTATE_TYPE,
          required: true,
          items: response.properties,
          initialExpanded: true,
        ));
        //</editor-fold>

        formMaker = FormMaker(form);
        update();
      },
      ({error, response}) {},
    );
  }

  fetchForm(String bargainId, String propertyId) {
    form = form.sublist(0, 1);

    EasyLoading.show(status: AppStrings.MESSAGE_LOADING);

    _paramRepository
        .filters(FiltersRequest(bargainId: bargainId, propertyId: propertyId),
            (response) {
      form.addAll(response.form);

      formMaker = FormMaker(form);

      update();
    }, ({error, response}) {
      print('on error');
    });
  }
}
