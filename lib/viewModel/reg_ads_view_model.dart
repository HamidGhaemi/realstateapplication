import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:rahnamaye_man/core/api/model/bargains_api.dart';
import 'package:rahnamaye_man/core/base/base_view_model.dart';
import 'package:rahnamaye_man/core/data/repository/param_repository.dart';
import 'package:rahnamaye_man/core/values/strings.dart';
import 'package:rahnamaye_man/model/key_value.dart';

class RegAdsViewModel extends BaseViewModel {
  ParamRepository _paramRepository = ParamRepository();

  List<KeyValue> bargains=[];
  KeyValue? selectedBargain;

  @override
  void onInit() {
    super.onInit();

    _fetchBargains();
  }

  _fetchBargains() {
    EasyLoading.show(status: AppStrings.MESSAGE_LOADING);

    _paramRepository.bargains(
      BargainsRequest(cityId: 189), //todo change cityId
      (response) {
        bargains = response.bargains;

        update();
      },
      ({error, response}) {
      },
    );
  }

  selectBargain(KeyValue bargain){
    selectedBargain = bargain;

    update();
  }
}
