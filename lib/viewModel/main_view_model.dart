import 'package:flutter/material.dart';
import 'package:rahnamaye_man/core/base/base_view_model.dart';

class MainViewModel extends BaseViewModel {
  late PageController pageController;
  int currentPage = 0;

  setCurrentPage(int index) {
    currentPage = index;
    pageController.animateToPage(index,
        duration: Duration(milliseconds: 300), curve: Curves.easeIn);

    update();
  }
}
