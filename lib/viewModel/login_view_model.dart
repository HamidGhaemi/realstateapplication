import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:rahnamaye_man/core/api/model/login_api.dart';
import 'package:rahnamaye_man/core/base/base_view_model.dart';
import 'package:rahnamaye_man/core/data/repository/param_repository.dart';
import 'package:rahnamaye_man/core/util/util.dart';
import 'package:rahnamaye_man/core/values/strings.dart';

class LoginViewModel extends BaseViewModel {
  ParamRepository _paramRepository = ParamRepository();

  _login(String username, String password) {
    EasyLoading.show(status: AppStrings.MESSAGE_LOADING);

    _paramRepository.login(LoginRequest(username: username, password: password),
        (response) {
      print('onSuccess');
    }, ({error, response}) {});
  }

  checkValidation(String username, String password) {
    if (username.length == 11 && password.isNotEmpty) {
      _login(username, password);
    } else {
      snackBar(AppStrings.MESSAGE_WARNING_VALIDATION);
    }
  }
}
