import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:persian_datetime_picker/persian_datetime_picker.dart';
import 'package:rahnamaye_man/component/components.dart';
import 'package:rahnamaye_man/core/util/util.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/dimens.dart';
import 'package:rahnamaye_man/core/values/strings.dart';
import 'package:rahnamaye_man/model/field.dart';
import 'package:rahnamaye_man/model/key_value.dart';

class FormMaker {
  List<Field> form = [];

  late VoidCallback? onUpdate, onValidate;
  late FormContinueCallback? onContinue;
  bool isValidationActive = false;

  FormMaker(this.form);

  make(
      {VoidCallback? onUpdate,
      FormContinueCallback? onContinue,
      VoidCallback? onValidate}) {
    this.onUpdate = onUpdate;
    this.onValidate = onValidate;
    this.onContinue = onContinue;

    return ListView.builder(
        itemCount: form.length,
        itemBuilder: (context, index) => _renderField(form[index]));
  }

  Widget _fieldTitle(String title) {
    return Text(
      title,
      style: TextStyle(color: AppColors.primary, fontWeight: FontWeight.bold),
    );
  }

  Widget _fieldSubtitle(String subtitle) {
    return Text(
      subtitle,
      style: TextStyle(color: AppColors.textColor2, fontSize: Dimens.textSize5),
    );
  }

  Widget _renderField(Field field) {
    Widget child;

    switch (field.type) {
      case 'radio':
      case 'select':
        child = _renderRadioField(field);
        break;

      case 'checkbox':
        child = _renderCheckBoxField(field);
        break;

      case 'multi_select':
        child = _renderMultiSelectField(field);
        break;

      case 'price':
        child = _renderPriceField(field);
        break;

      case 'num':
        child = _renderNumericField(field);
        break;

      case 'textarea':
        child = _renderTextAreaField(field);
        break;

      case 'date':
        child = _renderDateField(field);
        break;

      case 'dimension':
        child = _renderDimensionField(field);
        break;

      case 'num_picker':
        child = _renderNumberPickerField(field);
        break;

      default:
        child = SizedBox(height: 0);
        break;
    }

    return ExpansionTile(
      title: _fieldTitle(field.title!),
      subtitle: _fieldSubtitle(field.subtitle!),
      initiallyExpanded: field.initialExpanded!,
      leading: _fieldIcon(field),
      children: [
        child,
      ],
    );
  }

  Icon _fieldIcon(Field field) {
    IconData? iconData;
    Color? iconColor;

    if (isValidationActive) {
      if (field.required! && field.value != null) {
        iconData = Icons.check_circle;
        iconColor = AppColors.green;
      } else if (field.value == null) {
        iconData = Icons.cancel;
        iconColor = AppColors.red;
      } else {
        // is not required
        if (field.value != null) {
          iconData = Icons.check_circle;
          iconColor = AppColors.green;
        } else {
          iconData = Icons.panorama_fish_eye;
          iconColor = AppColors.gray;
        }
      }
    } else {
      if (field.required!) {
        iconData = Icons.info_rounded;
        iconColor = AppColors.orange;

        if (field.value != null) {
          iconData = Icons.check_circle;
          iconColor = AppColors.green;
        }
      } else {
        iconData = Icons.panorama_fish_eye;
        iconColor = AppColors.gray;

        if (field.value != null) {
          iconData = Icons.check_circle;
          iconColor = AppColors.green;
        }
      }
    }

    return Icon(iconData, color: iconColor);
  }

  Widget _renderRadioField(Field field) {
    return Column(
      children: [
        SizedBox(height: 16),
        GridView.builder(
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              childAspectRatio: 3 / 1,
              crossAxisSpacing: 12,
              mainAxisSpacing: 12,
              maxCrossAxisExtent: 150),
          itemCount: field.items!.length,
          itemBuilder: (context, index) =>
              _selectableItem(field.items![index], field),
        ),
        SizedBox(
          height: Dimens.rootPadding,
        ),
      ],
    );
  }

  Widget _renderCheckBoxField(Field field) {
    return Column(
      children: [
        SizedBox(height: 16),
        GridView.builder(
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              childAspectRatio: 3 / 1,
              crossAxisSpacing: 12,
              mainAxisSpacing: 12,
              maxCrossAxisExtent: 150),
          itemCount: field.items!.length,
          itemBuilder: (context, index) =>
              _selectableItem(field.items![index], field),
        ),
        SizedBox(
          height: Dimens.rootPadding,
        ),
      ],
    );
  }

  Widget _renderMultiSelectField(Field field) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.all(15.0),
      padding: const EdgeInsets.all(3.0),
      decoration: BoxDecoration(
          border: Border.all(color: AppColors.textColor2),
          borderRadius: BorderRadius.circular(Dimens.fieldRadius)),
      child: Row(
        children: [
          Expanded(
            child: Text(field.toStr(field.value.toString())),
          ),
          InkWell(
            customBorder: CircleBorder(),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                Icons.add,
                color: AppColors.primary,
              ),
            ),
            onTap: () {
              showModalBottomSheet(
                  context: Get.context!,
                  builder: (context) {
                    return StatefulBuilder(
                        builder: (BuildContext context, StateSetter setState) {
                      return _multiSelectBottomSheet(field, setState);
                    });
                  });
            },
          ),
        ],
      ),
    );
  }

  Widget _renderPriceField(Field field) {
    return TextField(
      keyboardType: TextInputType.number,
      decoration: new InputDecoration(
        border: new OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.textColor2),
          borderRadius: BorderRadius.circular(10.0),
        ),
        suffixText: AppStrings.CURRENCY,
        contentPadding: EdgeInsets.all(8),
      ),
      maxLength: 19,
      onChanged: (price) {
        field.value = price;

        if (price.isEmpty) {
          field.value = null;
        }

        onUpdate!.call();
      },
    );
  }

  Widget _renderNumericField(Field field) {
    return TextField(
      keyboardType: TextInputType.number,
      decoration: new InputDecoration(
        border: new OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.textColor2),
          borderRadius: BorderRadius.circular(Dimens.fieldRadius),
        ),
        contentPadding: EdgeInsets.all(8),
      ),
      onChanged: (value) {
        field.value = value;

        if (value.isEmpty) {
          field.value = null;
        }

        onUpdate!.call();
      },
    );
  }

  Widget _renderTextAreaField(Field field) {
    return TextField(
      keyboardType: TextInputType.text,
      decoration: new InputDecoration(
        border: new OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.textColor2),
          borderRadius: BorderRadius.circular(Dimens.fieldRadius),
        ),
        contentPadding: EdgeInsets.all(8),
      ),
      minLines: 3,
      maxLines: 3,
      maxLength: 70,
      //fixme config it
      onChanged: (value) {
        field.value = value;

        if (value.isEmpty) {
          field.value = null;
        }

        onUpdate!.call();
      },
    );
  }

  Widget _renderDateField(Field field) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.all(15.0),
      padding: const EdgeInsets.all(3.0),
      decoration: BoxDecoration(
          border: Border.all(color: AppColors.textColor2),
          borderRadius: BorderRadius.circular(Dimens.fieldRadius)),
      child: InkWell(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
              child: Text(
            field.value ?? '--/--/----',
          )),
        ),
        onTap: () {
          showPersianDatePicker(
            context: Get.context!,
            helpText: '',
            initialDate: Jalali.now(),
            firstDate: Jalali(1300, 1),
            lastDate: Jalali(1401, 12),
          ).then((value) {
            field.value = value!.formatCompactDate();

            onUpdate!.call();
          });
        },
      ),
    );
  }

  Widget _renderDimensionField(Field field) {
    return Row(
      children: [
        Flexible(
          flex: 1,
          child: TextField(
            textAlign: TextAlign.center,
            keyboardType: TextInputType.number,
            decoration: new InputDecoration(
              border: new OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.textColor2),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(Dimens.fieldRadius),
                  bottomRight: Radius.circular(Dimens.fieldRadius),
                ),
              ),
              contentPadding: EdgeInsets.all(8),
              hintText: AppStrings.ESTATE_X,
            ),
            onChanged: (value) {
              field.value = value;

              if (value.isEmpty) {
                field.value = null;
              }

              onUpdate!.call();
            },
          ),
        ),
        Flexible(
          flex: 1,
          child: TextField(
            textAlign: TextAlign.center,
            keyboardType: TextInputType.number,
            decoration: new InputDecoration(
              border: new OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.textColor2),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(Dimens.fieldRadius),
                  bottomLeft: Radius.circular(Dimens.fieldRadius),
                ),
              ),
              contentPadding: EdgeInsets.all(8),
              hintText: AppStrings.ESTATE_Y,
            ),
            onChanged: (value) {
              field.value = value;

              if (value.isEmpty) {
                field.value = null;
              }

              onUpdate!.call();
            },
          ),
        ),
      ],
    );
  }

  Widget _renderNumberPickerField(Field field) {
    return Container(
      width: double.infinity,
      height: 45,
      decoration: BoxDecoration(
          border: Border.all(color: AppColors.textColor2),
          borderRadius: BorderRadius.circular(Dimens.fieldRadius)),
      child: PageView.builder(
          itemCount: field.items!.length,
          controller: PageController(viewportFraction: .2),
          itemBuilder: (context, index) {
            return InkWell(
              customBorder: CircleBorder(),
              child: Center(
                child: Container(
                  width: 35,
                  height: 35,
                  decoration: BoxDecoration(
                    color: field.items![index].selected
                        ? AppColors.primary
                        : Colors.transparent,
                    shape: BoxShape.circle,
                  ),
                  child: Center(
                    child: Text(
                        field.items![index]
                            .title![field.items![index].title!.length - 1],
                        style: TextStyle(
                            color: field.items![index].selected
                                ? AppColors.white
                                : AppColors.textColor2)),
                  ),
                ),
              ),
              onTap: () {
                //<editor-fold desc="select item">
                field.items!.forEach((element) {
                  element.selected = false;
                });

                field.items![index].selected = !field.items![index].selected;
                onUpdate!.call();
                //</editor-fold>
              },
            );
          }),
    );
  }

  Widget _selectableItem(KeyValue item, Field field) {
    return InkWell(
      child: Container(
        padding: EdgeInsets.all(4.0),
        decoration: BoxDecoration(
            color: item.selected ? AppColors.primary : AppColors.background,
            border: Border.all(
                width: item.selected ? 2 : 1,
                color: item.selected ? AppColors.primary : AppColors.gray),
            borderRadius:
                BorderRadius.all(Radius.circular(Dimens.fieldRadius))),
        child: Center(
            child: Text(
          item.title!,
          style: TextStyle(
              color: item.selected ? AppColors.white : AppColors.textColor2),
        )),
      ),
      onTap: () {
        //<editor-fold desc="select item">
        if (field.type == 'radio' || field.type == 'select') {
          field.items!.forEach((element) {
            element.selected = false;
          });
        }

        item.selected = !item.selected;
        //</editor-fold>

        switch (field.type) {
          case 'radio':
          case 'select':
            field.items!.forEach((element) {
              if (element.selected) {
                field.value = element.id.toString();
              }
            });
            break;

          case 'checkbox':
          case 'multi_select':
            bool isSelectedAnyItem = false;
            field.items!.forEach((element) {
              if (element.selected) {
                isSelectedAnyItem = true;

                if (field.value != null) {
                  if (field.value![field.value!.length - 1] != ',') {
                    field.value = field.value! + ',';
                  }
                } else {
                  field.value = "";
                }

                field.value = field.value! + element.id.toString();
              }
            });

            if (!isSelectedAnyItem) {
              field.value = null;
            }
            break;
        }

        if (field.id!.toString() == '0') {
          onContinue!.call(field.value!);
        }

        onUpdate!.call();
      },
    );
  }

  Widget _selectableItemWithTick(
      KeyValue item, Field field, StateSetter stateSetter) {
    return InkWell(
      child: Container(
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
              border: Border.all(
                  width: item.selected ? 2 : 1,
                  color: item.selected ? AppColors.primary : AppColors.gray),
              borderRadius:
                  BorderRadius.all(Radius.circular(Dimens.fieldRadius))),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  item.title!,
                  style: TextStyle(
                      color: item.selected
                          ? AppColors.primary
                          : AppColors.textColor2),
                ),
              ),
              Icon(item.selected ? Icons.check : Icons.add,
                  color:
                      item.selected ? AppColors.primary : AppColors.textColor2)
            ],
          )),
      onTap: () {
        //<editor-fold desc="select item">
        if (field.type == 'radio' || field.type == 'select') {
          field.items!.forEach((element) {
            element.selected = false;
          });
        }

        item.selected = !item.selected;
        //</editor-fold>

        switch (field.type) {
          case 'select':
            field.items!.forEach((element) {
              if (element.selected) {
                field.value = element.id.toString();
              }
            });
            break;

          case 'multi_select':
            bool isSelectedAnyItem = false;
            field.value = null;
            field.items!.forEach((element) {
              if (element.selected) {
                isSelectedAnyItem = true;

                if (field.value != null) {
                  if (field.value![field.value!.length - 1] != ',') {
                    field.value = field.value! + ',';
                  }
                } else {
                  field.value = '';
                }

                field.value = field.value! + element.id.toString();
              }
            });

            if (!isSelectedAnyItem) {
              field.value = null;
            }
            break;
        }

        if (field.id!.toString() == '0') {
          onContinue!.call(field.value!);
        }

        stateSetter.call(() {});
        onUpdate!.call();
      },
    );
  }

  Widget _multiSelectBottomSheet(Field field, StateSetter stateSetter) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(
              left: Dimens.rootPadding,
              right: Dimens.rootPadding,
              bottom: 8,
              top: Dimens.rootPadding),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _fieldTitle(field.title!),
                    _fieldSubtitle(field.subtitle!),
                  ],
                ),
              ),
              InkWell(
                child: Icon(Icons.cancel, color: AppColors.gray),
                onTap: () {
                  Navigator.pop(Get.context!);
                },
              ),
            ],
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(Dimens.rootPadding),
            child: SingleChildScrollView(
              child: GridView.builder(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    childAspectRatio: 3 / 1,
                    crossAxisSpacing: 12,
                    mainAxisSpacing: 12,
                    maxCrossAxisExtent: 170),
                itemCount: field.items!.length,
                itemBuilder: (context, index) {
                  return _selectableItemWithTick(
                      field.items![index], field, stateSetter);
                },
              ),
            ),
          ),
        ),
        primaryButton(AppStrings.CONFIRM, () {
          Navigator.pop(Get.context!);
        })
      ],
    );
  }

  validate() {
    isValidationActive = true;
    bool isValid = true;

    form.forEach((element) {
      if (element.required! && element.value == null) {
        isValid = false;
        snackBar(AppStrings.VALIDATION_REQUIRED_FIELD);
      }
    });

    if (isValid) onValidate!.call();

    isValidationActive = false;
  }
}

typedef FormContinueCallback = void Function(String value);
