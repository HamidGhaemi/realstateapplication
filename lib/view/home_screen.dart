import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:rahnamaye_man/core/base/base_stateless_widget.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/dimens.dart';
import 'package:rahnamaye_man/core/values/strings.dart';
import 'package:rahnamaye_man/core/values/styles.dart';
import 'package:rahnamaye_man/view/estate_details_screen.dart';
import 'package:rahnamaye_man/viewModel/home_view_model.dart';
import 'package:shape_of_view_null_safe/shape/arc.dart';
import 'package:shape_of_view_null_safe/shape_of_view_null_safe.dart';

class HomeScreen extends BaseStatelessWidget {
  late HomeViewModel vm;

  @override
  Widget build(BuildContext context) {
    vm = Get.put(HomeViewModel());

    return GetBuilder<HomeViewModel>(
      builder: (value) => Stack(
        children: [
          Scaffold(
            backgroundColor: AppColors.primary,
            body: Card(
              color: AppColors.background,
              elevation: 0,
              margin: EdgeInsets.zero,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              )),
              child: Padding(
                padding: const EdgeInsets.only(left: 0, right: 0, top: 16, bottom: 16),
                child: ListView.builder(
                  itemCount: 7,
                  itemBuilder: (context, index) => _estateItem(),
                ),
              ),
            ),
          ),
          ShapeOfView(
            shape:
                ArcShape(direction: ArcDirection.Outside, height: 35, position: ArcPosition.Bottom),
            child: Container(
              height: 160,
              color: AppColors.secondary,
              child: Column(
                children: [
                  Stack(
                    children: [
                      appBar(
                        AppStrings.APP_NAME,
                        actions: [
                          InkWell(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset(
                                'assets/icons/filter.svg',
                                color: AppColors.white,
                                width: Dimens.iconSize,
                                height: Dimens.iconSize,
                              ),
                            ),
                            customBorder: CircleBorder(),
                            onTap: () {},
                          ),
                          SizedBox(width: 16),
                        ],
                      ),
                      Row(
                        children: [
                          SizedBox(width: 12),
                          Column(
                            children: [
                              SizedBox(height: 30),
                              InkWell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: SvgPicture.asset(
                                    'assets/icons/menu.svg',
                                    color: AppColors.white,
                                    width: Dimens.iconSize,
                                    height: Dimens.iconSize,
                                  ),
                                ),
                                customBorder: CircleBorder(),
                                onTap: () {},
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 12, bottom: 16),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: TextButton(
                        style: ButtonStyle(
                          backgroundColor:
                          MaterialStateProperty.all<Color>(AppColors.white),
                        ),
                        onPressed: () {
                        },
                        child: Text(
                          AppStrings.CHOOSE_CITY,
                          style: TextStyle(color: AppColors.textColor2),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _estateItem() {
    return Padding(
      padding: const EdgeInsets.only(right: 12.0, left: 12, top: 6),
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        child: InkWell(
          onTap: ()=> Get.to(EstateDetailsScreen()),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(12), bottomRight: Radius.circular(12)),
                child: Image.network(
                  'https://www.talab.org/wp-content/uploads/2018/07/1262924109-talab-org.jpg',
                  width: 120,
                  height: 190,
                  fit: BoxFit.cover,
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'اجاره آپارتمان 100 متری',
                              style: TextStyle(
                                  fontSize: Dimens.textSize4,
                                  color: AppColors.textColor1,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Image.network(
                            'https://upload.wikimedia.org/wikipedia/commons/1/1e/RPC-JP_Logo.png',
                            width: 20,
                            height: 20,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            size: 16,
                            color: AppColors.textColor2,
                          ),
                          Text(
                            'آدرس سسذیبیتدبذسیرس',
                            style: TextStyle(fontSize: Dimens.textSize5, color: AppColors.textColor2),
                          ),
                        ],
                      ),
                      SizedBox(height: 8),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.king_bed_outlined,
                                    color: AppColors.textColor2,
                                  ),
                                  Text(
                                    '2 خواب',
                                    style: TextStyle(fontSize: Dimens.textSize6),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.king_bed_outlined,
                                    color: AppColors.textColor2,
                                  ),
                                  Text(
                                    '2 خواب',
                                    style: TextStyle(fontSize: Dimens.textSize6),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.king_bed_outlined,
                                    color: AppColors.textColor2,
                                  ),
                                  Text(
                                    '2 خواب',
                                    style: TextStyle(fontSize: Dimens.textSize6),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.king_bed_outlined,
                                    color: AppColors.textColor2,
                                  ),
                                  Text(
                                    '2 خواب',
                                    style: TextStyle(fontSize: Dimens.textSize6),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(6.0),
                        child: Text('ودیعه: 20,000,000'),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(6.0),
                        child: Text('ودیعه: 20,000,000'),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
