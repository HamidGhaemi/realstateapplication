import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:rahnamaye_man/component/components.dart';
import 'package:rahnamaye_man/core/base/base_stateless_widget.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/dimens.dart';
import 'package:rahnamaye_man/core/values/strings.dart';
import 'package:rahnamaye_man/viewModel/login_view_model.dart';

class LoginScreen extends BaseStatelessWidget {
  late LoginViewModel vm;
  bool isShowPassword = false;

  var _usernameController = TextEditingController();
  var _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    vm = Get.put(LoginViewModel());

    return GetBuilder<LoginViewModel>(
        builder: (value) => Scaffold(
              body: Column(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 60,
                        ),
                        Text(
                          AppStrings.LOGIN_FORM_TITLE,
                          style: TextStyle(
                              color: AppColors.primary,
                              fontSize: Dimens.textSize1),
                        ),
                        Text(
                          AppStrings.MESSAGE_LOGIN_FORM_TITLE,
                          style: TextStyle(
                              color: AppColors.textColor2,
                              fontSize: Dimens.textSize4),
                        ),
                        SizedBox(
                          height: 100,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(32),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: AppColors.gray)),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8, right: 8),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.phone_android,
                                    color: AppColors.primary,
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                    child: TextField(
                                        controller: _usernameController,
                                        keyboardType: TextInputType.number,
                                        maxLength: 11,
                                        decoration: new InputDecoration(
                                            border: InputBorder.none,
                                            focusedBorder: InputBorder.none,
                                            counterText: "",
                                            enabledBorder: InputBorder.none,
                                            errorBorder: InputBorder.none,
                                            disabledBorder: InputBorder.none,
                                            hintText: AppStrings.HINT_ENTER_NUMBER)),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 18, left: 32, right: 32),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: AppColors.gray)),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8, right: 8),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.lock_outline,
                                    color: AppColors.primary,
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                    child: TextField(
                                        controller: _passwordController,
                                        keyboardType:
                                            TextInputType.visiblePassword,
                                        obscureText: !isShowPassword,
                                        enableSuggestions: false,
                                        autocorrect: false,
                                        decoration: new InputDecoration(
                                            border: InputBorder.none,
                                            focusedBorder: InputBorder.none,
                                            counterText: "",
                                            enabledBorder: InputBorder.none,
                                            errorBorder: InputBorder.none,
                                            disabledBorder: InputBorder.none,
                                            hintText: AppStrings
                                                .HINT_ENTER_PASSWORD)),
                                  ),
                                  InkWell(
                                    customBorder: CircleBorder(),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(
                                        isShowPassword
                                            ? Icons.visibility
                                            : Icons.visibility_off,
                                        color: AppColors.primary,
                                      ),
                                    ),
                                    onTap: () {
                                      isShowPassword = !isShowPassword;
                                      vm.update();
                                    },
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        InkWell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              AppStrings.MESSAGE_FORGET_PASSWORD,
                              style: TextStyle(
                                  color: AppColors.primary,
                                  fontSize: Dimens.textSize4),
                            ),
                          ),
                          onTap: () {

                          },
                        ),
                      ],
                    ),
                  ),
                  primaryButton(AppStrings.ENTER_TO_APP, () {
                    vm.checkValidation(_usernameController.text,
                        _passwordController.text);
                  })
                ],
              ),
            ));
  }
}
