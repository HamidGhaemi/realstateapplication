import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:rahnamaye_man/core/base/base_stateless_widget.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/dimens.dart';
import 'package:rahnamaye_man/core/values/strings.dart';
import 'package:rahnamaye_man/viewModel/login_view_model.dart';
import 'package:rahnamaye_man/viewModel/splash_view_model.dart';

class SplashScreen extends BaseStatelessWidget {
  late SplashViewModel vm;

  @override
  Widget build(BuildContext context) {
    vm = Get.put(SplashViewModel());

    Future.delayed(const Duration(milliseconds: 500), () {
      vm.goNextScreen();
    });

    return GetBuilder<LoginViewModel>(
        builder: (value) => Scaffold(
              body: Column(
                children: [
                  Text(
                    AppStrings.LOGIN_FORM_TITLE,
                    style: TextStyle(
                        color: AppColors.primary, fontSize: Dimens.textSize1),
                  )
                ],
              ),
            ));
  }
}
