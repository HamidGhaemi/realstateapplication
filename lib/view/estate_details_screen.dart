import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:rahnamaye_man/core/base/base_stateless_widget.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/dimens.dart';
import 'package:rahnamaye_man/core/values/strings.dart';
import 'package:rahnamaye_man/core/values/styles.dart';
import 'package:rahnamaye_man/viewModel/estate_details_view_model.dart';

class EstateDetailsScreen extends BaseStatelessWidget {
  late EstateDetailsViewModel vm;

  @override
  Widget build(BuildContext context) {
    vm = Get.put(EstateDetailsViewModel());

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: AppColors.primary,
      statusBarIconBrightness: Brightness.light,
    ));

    final CarouselController _controller = CarouselController();

    return GetBuilder<EstateDetailsViewModel>(
        builder: (builder) => SafeArea(
                child: Scaffold(
              backgroundColor: AppColors.primary,
              appBar: appBar('فروش آپارتمان 92 متری شهید بهشتی'),
              body: Card(
                color: AppColors.background,
                elevation: 0,
                margin: EdgeInsets.zero,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                )),
                child: Column(
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            Stack(
                              children: [
                                Stack(
                                  children: [
                                    ClipRRect(
                                      borderRadius:
                                          BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30)),
                                      child: CarouselSlider(
                                        carouselController: _controller,
                                        options: CarouselOptions(
                                            aspectRatio: 4 / 3,
                                            viewportFraction: 1.0,
                                            enlargeCenterPage: true,
                                            autoPlay: true,
                                            onPageChanged: (index, __) {
                                              vm.setSliderIndex(index);
                                            }),
                                        items: [1, 2, 3, 4, 5].map((i) {
                                          return Builder(
                                            builder: (BuildContext context) {
                                              return Container(
                                                width: MediaQuery.of(context).size.width,
                                                margin: EdgeInsets.symmetric(horizontal: 0),
                                                child: Image.network(
                                                  'https://thesimscatalog.com/sims4/wp-content/uploads/2018/11/8ebf219cee1a1efbf3c7bc3aa1633e88.jpeg',
                                                  fit: BoxFit.cover,
                                                ),
                                              );
                                            },
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                      child: Padding(
                                        padding: const EdgeInsets.only(bottom: 32),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [1, 2, 3, 4, 5].asMap().entries.map((entry) {
                                            return GestureDetector(
                                              onTap: () => _controller.animateToPage(entry.key),
                                              child: Container(
                                                width: 8,
                                                height: 8,
                                                margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color:
                                                        (Colors.white).withOpacity(vm.currentSlide == entry.key ? 0.9 : 0.4)),
                                              ),
                                            );
                                          }).toList(),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Container(
                                        height: 300,
                                        child: Padding(
                                          padding: const EdgeInsets.all(12),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Card(
                                                  color: AppColors.primary,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.all(Radius.circular(30))),
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: Icon(
                                                      Icons.videocam,
                                                      color: AppColors.white,
                                                    ),
                                                  )),
                                              SizedBox(height: 16),
                                              Card(
                                                  color: AppColors.primary,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.all(Radius.circular(30))),
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: SvgPicture.asset(
                                                      'assets/icons/vr.svg',
                                                      width: 24,
                                                      height: 24,
                                                      color: AppColors.white,
                                                    ),
                                                  )),
                                              SizedBox(height: 16),
                                              Card(
                                                  color: AppColors.primary,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.all(Radius.circular(30))),
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: Icon(
                                                      Icons.favorite,
                                                      color: AppColors.white,
                                                    ),
                                                  )),
                                              SizedBox(height: 16),
                                              Card(
                                                  color: AppColors.primary,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.all(Radius.circular(30))),
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: Icon(
                                                      Icons.share,
                                                      color: AppColors.white,
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            color: Color(0x55FFFFFF),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.only(bottom: 2, top: 2, left: 8, right: 8),
                                            child: Wrap(
                                              crossAxisAlignment: WrapCrossAlignment.center,
                                              children: [
                                                Icon(
                                                  Icons.location_pin,
                                                  color: AppColors.white,
                                                  size: 16,
                                                ),
                                                SizedBox(width: 8),
                                                Text(
                                                  'تهران - ورامین - شهر رضا - خونه رضا',
                                                  style: TextStyle(color: AppColors.white),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Column(
                                  children: [
                                    SizedBox(
                                      height: MediaQuery.of(context).size.width * 3 / 4 - 30,
                                    ),
                                    Card(
                                      margin: const EdgeInsets.only(right: 12, left: 12),
                                      elevation: 4,
                                      child: Padding(
                                        padding: const EdgeInsets.all(12),
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Row(
                                                    children: [
                                                      Card(
                                                        shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(15),
                                                        ),
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                                                          child: Text(
                                                            'خرید و فروش',
                                                            style:
                                                                TextStyle(color: AppColors.white, fontSize: Dimens.textSize5),
                                                          ),
                                                        ),
                                                        color: AppColors.primary,
                                                      ),
                                                      SizedBox(width: 4),
                                                      Card(
                                                        shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(15),
                                                        ),
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                                                          child: Text(
                                                            'آپارتمان',
                                                            style:
                                                                TextStyle(color: AppColors.white, fontSize: Dimens.textSize5),
                                                          ),
                                                        ),
                                                        color: Color(0xFF4D5761),
                                                      ),
                                                      SizedBox(width: 4),
                                                      Card(
                                                        shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(15),
                                                        ),
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                                                          child: Text(
                                                            'مسکونی',
                                                            style:
                                                                TextStyle(color: AppColors.white, fontSize: Dimens.textSize5),
                                                          ),
                                                        ),
                                                        color: Color(0xFF4D5761),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Card(
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.circular(15),
                                                  ),
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                                                    child: Text(
                                                      'کد: 1232',
                                                      style: TextStyle(color: AppColors.white, fontSize: Dimens.textSize5),
                                                    ),
                                                  ),
                                                  color: AppColors.red,
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 16),
                                            Row(
                                              children: [
                                                Expanded(
                                                  flex: 1,
                                                  child: Row(
                                                    children: [
                                                      Text(
                                                        AppStrings.PRICE_,
                                                        style: TextStyle(
                                                            fontSize: Dimens.textSize4, color: AppColors.textColor2),
                                                      ),
                                                      SizedBox(width: 4),
                                                      Text(
                                                        '1,700,000,000 تومان',
                                                        style: TextStyle(
                                                            fontSize: Dimens.textSize4, color: AppColors.textColor1),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 1,
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    children: [
                                                      Text(
                                                        AppStrings.PRICE_OF_METER_,
                                                        style: TextStyle(
                                                            fontSize: Dimens.textSize4, color: AppColors.textColor2),
                                                      ),
                                                      SizedBox(width: 4),
                                                      Text(
                                                        '11,700,000 تومان',
                                                        style: TextStyle(
                                                            fontSize: Dimens.textSize4, color: AppColors.textColor1),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(12),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child: Center(
                                              child: Column(
                                                children: [
                                                  Icon(
                                                    Icons.king_bed_outlined,
                                                    color: AppColors.gray,
                                                    size: 40,
                                                  ),
                                                  SizedBox(height: 4),
                                                  Text(
                                                    '2 خواب',
                                                    style: TextStyle(color: AppColors.textColor2, fontSize: Dimens.textSize4),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 1,
                                            child: Center(
                                              child: Column(
                                                children: [
                                                  Icon(
                                                    Icons.horizontal_rule_outlined,
                                                    color: AppColors.gray,
                                                    size: 40,
                                                  ),
                                                  SizedBox(height: 4),
                                                  Text(
                                                    '150 متر',
                                                    style: TextStyle(color: AppColors.textColor2, fontSize: Dimens.textSize4),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 1,
                                            child: Center(
                                              child: Column(
                                                children: [
                                                  Icon(
                                                    Icons.document_scanner_outlined,
                                                    color: AppColors.gray,
                                                    size: 40,
                                                  ),
                                                  SizedBox(height: 4),
                                                  Text(
                                                    'شش دانگ',
                                                    style: TextStyle(color: AppColors.textColor2, fontSize: Dimens.textSize4),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 1,
                                            child: Center(
                                              child: Column(
                                                children: [
                                                  Icon(
                                                    Icons.calendar_today_outlined,
                                                    color: AppColors.gray,
                                                    size: 40,
                                                  ),
                                                  SizedBox(height: 4),
                                                  Text(
                                                    '12 ساله',
                                                    style: TextStyle(color: AppColors.textColor2, fontSize: Dimens.textSize4),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      color: AppColors.primary,
                                      child: Padding(
                                        padding: const EdgeInsets.all(12),
                                        child: Row(
                                          children: [
                                            Container(
                                                decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                                                child: Padding(
                                                  padding: const EdgeInsets.all(4),
                                                  child: Icon(
                                                    Icons.equalizer_outlined,
                                                    color: AppColors.primary,
                                                  ),
                                                )),
                                            Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Text(
                                                'نظر کارشناس درباره این ملک',
                                                style: TextStyle(color: AppColors.white, fontSize: Dimens.textSize6),
                                              ),
                                            ),
                                            Expanded(
                                              child: LinearPercentIndicator(
                                                animation: true,
                                                animationDuration: 1000,
                                                lineHeight: 5,
                                                percent: 0.4,
                                                linearStrokeCap: LinearStrokeCap.roundAll,
                                                progressColor: Colors.white,
                                              ),
                                            ),
                                            SizedBox(width: 8),
                                            Container(
                                                decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                                                child: Padding(
                                                  padding: const EdgeInsets.all(4),
                                                  child: Icon(
                                                    Icons.play_arrow_rounded,
                                                    color: AppColors.primary,
                                                  ),
                                                )),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(12),
                                      child: Card(
                                        child: Padding(
                                          padding: const EdgeInsets.all(12),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'مشخصات داخلی',
                                                style: TextStyle(color: AppColors.primary, fontSize: Dimens.textSize5),
                                              ),
                                              SizedBox(height: 8),
                                              GridView.builder(
                                                shrinkWrap: true,
                                                physics: BouncingScrollPhysics(),
                                                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                                                  childAspectRatio: 4 / 1,
                                                  maxCrossAxisExtent: 150,
                                                ),
                                                itemCount: 6,
                                                itemBuilder: (context, index) {
                                                  return Row(
                                                    children: [
                                                      Icon(
                                                        Icons.circle,
                                                        size: 12,
                                                        color: AppColors.primary,
                                                      ),
                                                      SizedBox(width: 8),
                                                      Text('کابینت',style: TextStyle(color: AppColors.textColor1,fontSize: Dimens.textSize5),)
                                                    ],
                                                  );
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      color: AppColors.primary,
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Row(
                          children: [
                            Container(
                              width: 40.0,
                              height: 40.0,
                              decoration: BoxDecoration(
                                color: AppColors.white,
                                image: DecorationImage(
                                  image: NetworkImage('https://ahmadazarnia.ir/wp-content/uploads/2019/10/pp-140x140.jpeg'),
                                  fit: BoxFit.cover,
                                ),
                                borderRadius: BorderRadius.all( Radius.circular(20.0)),
                                border: Border.all(
                                  color: Colors.white,
                                  width: 1.5,
                                ),
                              ),
                            ),
                            Expanded(child: Text('تماس با کارشناس این ملک (جهرمی)',textAlign: TextAlign.center, style: TextStyle(color: AppColors.white),)),
                            Card(
                              color: AppColors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(30))),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Icon(
                                  Icons.call,
                                  color: AppColors.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )));
  }
}
