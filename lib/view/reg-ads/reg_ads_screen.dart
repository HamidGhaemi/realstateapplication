import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:rahnamaye_man/component/components.dart';
import 'package:rahnamaye_man/core/base/base_stateless_widget.dart';
import 'package:rahnamaye_man/core/util/util.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/dimens.dart';
import 'package:rahnamaye_man/core/values/strings.dart';
import 'package:rahnamaye_man/core/values/styles.dart';
import 'package:rahnamaye_man/model/key_value.dart';
import 'package:rahnamaye_man/view/reg-ads/reg_ads_step2_screen.dart';
import 'package:rahnamaye_man/viewModel/reg_ads_view_model.dart';

class RegAdsScreen extends BaseStatelessWidget {
  late RegAdsViewModel vm;

  @override
  Widget build(BuildContext context) {
    vm = Get.put(RegAdsViewModel());

    return GetBuilder<RegAdsViewModel>(
        builder: (value) => Scaffold(
              backgroundColor: AppColors.primary,
              appBar: appBar(AppStrings.REG_YOUR_ESTATE_ADS),
              body: Card(
                color: AppColors.background,
                elevation: 0,
                margin: EdgeInsets.zero,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                )),
                child: Column(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(Dimens.rootPadding),
                        child: Column(
                          children: [
                            SizedBox(height: 16),
                            Image.asset('assets/images/reg_ads_header.png'),
                            Card(
                              child: InkWell(
                                child: Padding(
                                  padding: const EdgeInsets.all(12),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SvgPicture.asset(
                                        'assets/icons/location.svg',
                                        color: AppColors.primary,
                                        width: Dimens.iconSize,
                                        height: Dimens.iconSize,
                                      ),
                                      SizedBox(width: 8),
                                      Text(AppStrings.MESSAGE_CHOOSE_AREA)
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  //fixme
                                },
                              ),
                            ),
                            SizedBox(height: 16),
                            Text(
                              AppStrings.MESSAGE_CHOOSE_BARGAIN,
                              style: TextStyle(color: AppColors.primary),
                            ),
                            SizedBox(height: 20),
                            GridView.builder(
                              shrinkWrap: true,
                              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                                  childAspectRatio: 4 / 1,
                                  crossAxisSpacing: 12,
                                  mainAxisSpacing: 12,
                                  maxCrossAxisExtent: 300),
                              itemCount: vm.bargains.length,
                              itemBuilder: (context, index) => _item(vm.bargains[index]),
                            ),
                          ],
                        ),
                      ),
                    ),
                    primaryButton(AppStrings.NEXT_IN_REG_ADS, () {
                      if (vm.selectedBargain == null) {
                        snackBar(AppStrings.VALIDATION_SELECT_BARGAIN);

                        return;
                      }

                      Get.to(RegAdsStep2Screen(vm.selectedBargain!));
                    })
                  ],
                ),
              ),
            ));
  }

  Widget _item(KeyValue bargain) {
    return InkWell(
      child: Container(
        padding: const EdgeInsets.all(3.0),
        decoration: BoxDecoration(
            color: vm.selectedBargain != null && vm.selectedBargain!.id == bargain.id
                ? AppColors.primary
                : AppColors.background,
            border: Border.all(
                width: vm.selectedBargain != null && vm.selectedBargain!.id == bargain.id ? 2 : 1,
                color: vm.selectedBargain != null && vm.selectedBargain!.id == bargain.id
                    ? AppColors.primary
                    : AppColors.gray),
            borderRadius: BorderRadius.all(Radius.circular(Dimens.fieldRadius))),
        child: Center(
            child: Text(
          bargain.title!,
          style: TextStyle(
              color: vm.selectedBargain != null && vm.selectedBargain!.id == bargain.id
                  ? AppColors.white
                  : AppColors.textColor2),
        )),
      ),
      onTap: () => vm.selectBargain(bargain),
    );
  }
}
