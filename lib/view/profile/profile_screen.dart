import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:rahnamaye_man/core/base/base_stateless_widget.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/dimens.dart';
import 'package:rahnamaye_man/view/profile/messages_screen.dart';
import 'package:rahnamaye_man/view/profile/requests_screen.dart';
import 'package:rahnamaye_man/viewModel/profile_view_model.dart';
import 'package:shape_of_view_null_safe/shape_of_view_null_safe.dart';

class ProfileScreen extends BaseStatelessWidget {
  late ProfileViewModel vm;

  @override
  Widget build(BuildContext context) {
    vm = Get.put(ProfileViewModel());

    return GetBuilder<ProfileViewModel>(
      builder: (value) => SafeArea(
        child: Scaffold(
          backgroundColor: AppColors.background,
          body: Column(
            children: [
              ShapeOfView(
                shape: ArcShape(direction: ArcDirection.Outside, height: 25, position: ArcPosition.Bottom),
                child: Container(
                  height: 230,
                  width: double.infinity,
                  color: AppColors.secondary,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 16,
                      ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(60.0),
                        child: Image.network(
                          'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
                          height: 120.0,
                          width: 120.0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'احمد کاشانی',
                          style: TextStyle(color: AppColors.white, fontSize: Dimens.textSize2),
                        ),
                      ),
                      Text(
                        'متخصص فنی لوله کش',
                        style: TextStyle(color: AppColors.white, fontSize: Dimens.textSize5),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: 16),
              Expanded(
                child: GridView.builder(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  gridDelegate:
                      SliverGridDelegateWithMaxCrossAxisExtent(childAspectRatio: 1 / 1, maxCrossAxisExtent: 200),
                  itemCount: 7,
                  itemBuilder: (context, index) {
                    return _profileItem(index);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _profileItem(int index) {
    return Padding(
      padding: const EdgeInsets.only(right: 5, left: 5, top: 6),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        elevation: 6,
        child: InkWell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  _itemImage(index),
                  size: 48,
                  color: AppColors.primary,
                ),
                SizedBox(height: 8),
                Text(
                  _itemTitle(index),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: AppColors.primary, fontSize: Dimens.textSize5),
                ),
              ],
            ),
          ),
          onTap: () {
            switch (index) {
              case 0:
                Get.to(MessagesScreen());
                break;
              case 1:
                Get.to(RequestsScreen());
                break;
            }
          },
        ),
      ),
    );
  }

  String _itemTitle(int index) {
    switch (index) {
      case 0:
        return 'صندوق دریافت پیام';
      case 1:
        return 'درخواست های من';
      case 2:
        return 'مورد علاقه های من';
      case 3:
        return 'ارسال مدارک هویتی';
      case 4:
        return 'ارسال نظرات کاربران';
      case 5:
        return 'رزومه کاری متخصص';
      case 6:
        return 'تنظیمات پروفایل';
    }
    return '';
  }

  IconData _itemImage(int index) {
    switch (index) {
      case 0:
        return Icons.email_outlined;
      case 1:
        return Icons.line_weight_sharp;
      case 2:
        return Icons.favorite_border;
      case 3:
        return Icons.mark_email_read_outlined;
      case 4:
        return Icons.mode_comment_outlined;
      case 5:
        return Icons.work_outline_rounded;
      case 6:
        return Icons.settings;
    }

    return Icons.email_outlined;
  }
}
